# Set-up integration tests for CaosDB.jl

Start a CaosDB server, for now in `f-grpc-dev` branch and set the
`CAOSDB_SERVER_HOST`, `CAOSDB_SERVER_GRPC_PORT_HTTPS`,
`CAOSDB_SERVER_CERT`, `CAOSDB_USER`, and `CAOSDB_PASSWORD`
environmental variables in such a way that a connection can be established with CaosDB.jl by

```julia
using CaosDB
CaosDB.Connection.connect_manually()
```

i.e., without any explicit arguments to the call to `connect_manually`. 

Also set up a `caosdb_client.json` with
a connection named `julialib-integrationtest` as default that connects
to the above server. See the [documentation of
libcaosdb](https://docs.indiscale.com/caosdb-cpplib/) for more
information on the config file.

# Tests

## Testing against a remote CaosDB.jl
This will fetch a version of CaosDB.jl from the remote server and use it. For 
an alternative using a local version see below.

After set-up, the integration tests can be executed by

```julia-repl
julia> ]activate "/path/to/caosdb-juliainttest"
julia> ]test
```

This will test them with the remote `CaosDB.jl` in a branch as
specified in `src/CaosDBIntegrationTests.jl`, which has the main
purpose of automatic desting in the gitlab CI.

## Testing your local CaosDB.jl

If you want to run the integration tests against your local
`CaosDB.jl` installation, either use the `runtest.jl` outside of the
package management, i.e., by executing

```sh
julia test/runtests.jl
```

(note that without specifing the verbosity, succeeding tests will not
generate output, only failing tests will) or you add the local
`CaosDB.jl` to the test dependencies manually and run the tests
afterwards:

```julia-repl
julia> ] activate ./test
julia> ] add path/to/local/caosdb-julialib
julia> ] activate .
julia> ] test
```
