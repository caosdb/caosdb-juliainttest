# ** header v3.0
# This file is a part of the CaosDB Project.
#
# Copyright (C) 2021 Indiscale GmbH <info@indiscale.com>
# Copyright (C) 2021 Florian Spreckelsen <f.spreckelsen@indiscale.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public
# License along with this program. If not, see
# <https://www.gnu.org/licenses/>.
#
# ** end header
#
# Start by installing CaosDB.jl in the correct branch

"""
This module only has the puprose of setting up the integration tests
with the correct version (and branch) of CaosDB.jl.
"""
module CaosDBIntegrationTests

repo_url = "https://gitlab.indiscale.com/caosdb/src/caosdb-julialib.git"

# Get possible special branch name
if haskey(ENV, "F_BRANCH")
    rev = ENV["F_BRANCH"]
else
    rev = ""
end

# Use default branch if no specific branch has been given
if rev == ""
    rev = nothing
end

end # module
