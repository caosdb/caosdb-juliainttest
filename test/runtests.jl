# ** header v3.0
# This file is a part of the CaosDB Project.
#
# Copyright (C) 2021 Indiscale GmbH <info@indiscale.com>
# Copyright (C) 2021 Florian Spreckelsen <f.spreckelsen@indiscale.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public
# License along with this program. If not, see
# <https://www.gnu.org/licenses/>.
#
# ** end header
#

using Test
using Logging
using UUIDs
# first try and load a local CaosDB installation; if that fails,
# set-up CaosDB.jl with repo and branch specified in
# `CaosDBIntegrationTests`. The latter is mainly useful for running
# this in a pipeline.
try
    using CaosDB
catch e
    if isa(e, ArgumentError)
        # Install from repo if it cannot be loaded
        using CaosDBIntegrationTests
        repo_url = CaosDBIntegrationTests.repo_url
        rev = CaosDBIntegrationTests.rev
        println("Using remote CaosDB.jl from $repo_url in branch $rev.")
        using Pkg
        try
            # Try first with the current rev
            Pkg.add(url = repo_url, rev = rev)
        catch e
            if isa(e, Pkg.Types.PkgError)
                @warn "Couldn't find remote branch $rev, using dev branch instead."
                Pkg.add(url = repo_url, rev = "dev")
            else
                throw(e)
            end
        end
        using CaosDB
    else
        # throw any other possible error
        throw(e)
    end
end

@testset "Test CaosDB" begin

    @testset "Test connection" begin
        # test whether connection can be established without an error
        @test CaosDB.Connection.connect() != nothing
        @test CaosDB.Connection.connect("julialib-integrationtest") != nothing
        # Test manually
        @test CaosDB.Connection.connect_manually() != nothing
    end

    @testset "Test transaction" begin

        # transactions can be created from connection names or objects
        connection = CaosDB.Connection.connect()
        @test create_transaction(connection) != nothing

        @test create_transaction() != nothing
        @test create_transaction("julialib-integrationtest") != nothing

        # Construct and execute a transaction manually
        transaction = create_transaction()
        add_query(transaction, "FIND ENTITY WITH id=-1")
        execute(transaction)
        result_set = get_result_set(transaction)
        results = get_results(result_set)
        # There is no entity with id=-1
        @test length(results) == 0
        # result-sets can be omitted
        transaction_results = get_results(transaction)
        @test length(transaction_results) == 0
        # No count query, so this should be -1
        @test get_count_result(transaction) == -1

        # Also works directly, with or without connection being
        # specified.
        query_results1 = execute_query("FIND ENTITY WITH id=-1")
        @test length(query_results1) == 0
        query_results2 = execute_query("FIND ENTITY WITH id=-1", "julialib-integrationtest")
        @test length(query_results2) == 0
        query_results3 = execute_query("FIND ENTITY WITH id=-1", connection)
        @test length(query_results3) == 0

        # COUNT queries have to be conducted manually
        count_transaction = create_transaction()
        add_query(count_transaction, "COUNT ENTITY WITH id=-1")
        execute(count_transaction)
        # Still an empty result set
        @test get_count_result(count_transaction) == 0
    end

    @testset "Test entity retrieval" begin

        single_retrieve_transaction = create_transaction()
        add_retrieve_by_id(single_retrieve_transaction, "20")
        execute(single_retrieve_transaction)
        results = get_results(single_retrieve_transaction)
        @test length(results) == 1
        @test has_errors(results[1]) == false
        @test get_name(results[1]) == "name"

        multi_retrieve_transaction = create_transaction()
        add_retrieve_by_id(multi_retrieve_transaction, ["20", "21"])
        execute(multi_retrieve_transaction)
        results = get_results(multi_retrieve_transaction)
        @test length(results) == 2
        @test get_name(results[1]) == "name"
        @test get_name(results[2]) == "unit"

        # Helper functions
        ent = retrieve("20")
        GC.gc() # assure that ent still has a valid wrapped pointer
        @test get_name(ent) == "name"

        results = retrieve(["20", "21"])
        @test length(results) == 2
        @test get_name(results[1]) == "name"
        @test get_name(results[2]) == "unit"

        # id=22 doesn't exist
        @test_throws CaosDB.Exceptions.GenericCaosDBException retrieve("22")
        @test_throws CaosDB.Exceptions.GenericCaosDBException retrieve(["20", "21", "22"])
    end

    @testset "Test entity insertion, update and deletion" begin
        # 1. Insert
        ent_with_name = CaosDB.Entity.create_recordtype("TestEnt")
        insert_rt_transaction = create_transaction()
        add_insert_entity(insert_rt_transaction, ent_with_name)
        execute(insert_rt_transaction)
        results = get_results(insert_rt_transaction)

        @test length(results) == 1
        @test has_errors(results[1]) == false
        @test get_id(results[1]) != ""

        # 2. update
        ent_with_name = execute_query("FIND TestEnt")
        prop = CaosDB.Entity.create_property_entity(
            name = "TestProperty",
            datatype = TEXT,
            unit = "cm",
        )

        insert_prop_transaction = create_transaction()
        add_insert_entity(insert_prop_transaction, prop)
        execute(insert_prop_transaction)
        results = get_results(insert_prop_transaction)

        retrieve_prop_transaction = create_transaction()
        add_retrieve_by_id(retrieve_prop_transaction, get_id(results[1]))
        execute(retrieve_prop_transaction)
        results = get_results(retrieve_prop_transaction)

        @test length(results) == 1
        @test has_errors(results[1]) == false
        @test get_name(results[1]) == "TestProperty"
        @test get_unit(results[1]) == "cm"
        @test get_datatype(results[1]) == (TEXT, nothing)

        prop2 = CaosDB.Entity.create_property(id = get_id(results[1]))
        CaosDB.Entity.append_property(ent_with_name[1], prop2)

        update_transaction = create_transaction()
        add_update_entity(update_transaction, ent_with_name[1])
        execute(update_transaction)
        results = get_results(update_transaction)
        @test length(results) == 1


        retrieve_transaction = create_transaction()
        add_retrieve_by_id(retrieve_transaction, get_id(results[1]))
        execute(retrieve_transaction)
        results = get_results(retrieve_transaction)

        @test length(results) == 1
        @test has_errors(results[1]) == false
        @test length(CaosDB.Entity.get_properties(results[1])) == 1
        prop = CaosDB.Entity.get_property(results[1], 1)
        @test get_name(prop) == "TestProperty"
        @test get_unit(prop) == "cm"

        # 3. delete
        ent_with_name = execute_query("FIND TestEnt")
        delete_transaction = create_transaction()
        add_delete_by_id(delete_transaction, get_id(ent_with_name[1]))
        execute(delete_transaction)
        results = get_results(delete_transaction)

        @test length(results) == 1
        @test has_errors(results[1]) == false
        @test get_id(results[1]) != ""

        # 4. Convenience functions
        ent_with_name = CaosDB.Entity.create_recordtype("TestEnt")
        results = insert_entity(ent_with_name)

        @test length(results) == 1
        @test has_errors(results[1]) == false
        @test get_id(results[1]) != ""

        ent_with_name = execute_query("FIND TestEnt")
        prop_with_name = execute_query("Find TestProperty")
        prop = CaosDB.Entity.create_property(id = get_id(prop_with_name[1]))
        CaosDB.Entity.append_property(ent_with_name[1], prop)
        results = update_entity(ent_with_name[1])

        @test length(results) == 1
        @test has_errors(results[1]) == false
        @test get_id(results[1]) != ""

        ent_with_name = execute_query("FIND TestEnt")
        results = delete_by_id(get_id(ent_with_name[1]))

        @test length(results) == 1
        @test has_errors(results[1]) == false
        @test get_id(results[1]) != ""

        # 5. Cleanup
        ent_with_name = execute_query("FIND TestProperty")
        cleanup_transaction = create_transaction()
        add_delete_by_id(cleanup_transaction, get_id(ent_with_name[1]))
        execute(cleanup_transaction)

    end

    @testset "Test upload and download of a File" begin
        # 1. Setup
        fname = string(uuid4()) # file name
        touch(fname)
        write(fname, "Some Content") # cannot be empty
        upload_path = string(pwd(), "/", fname)
        download_path = string(pwd(), "/", string(uuid4()))
        download_path2 = string(pwd(), "/", string(uuid4()), "-2")

        # 2. Upload Entity
        ent_with_name = CaosDB.Entity.create_entity("TestFile")
        set_role(ent_with_name, FILE)
        set_local_path(ent_with_name, upload_path)
        set_remote_path(ent_with_name, string("/Inttests/", fname))

        single_insert_transaction = create_transaction()

        add_insert_entity(single_insert_transaction, ent_with_name)
        execute(single_insert_transaction)
        results = get_results(single_insert_transaction)

        @test length(results) == 1
        @test has_errors(results[1]) == false
        @test get_id(results[1]) != ""

        # 3. Download entity
        ent_with_name = execute_query("FIND TestFile")
        single_insert_transaction = create_transaction()
        add_retrieve_and_download_file_by_id(
            single_insert_transaction,
            get_id(ent_with_name[1]),
            download_path,
        )
        execute(single_insert_transaction)
        results = get_results(single_insert_transaction)

        @test length(results) == 1
        @test has_errors(results[1]) == false
        @test get_id(results[1]) != ""
        @test isfile(download_path) == true

        # 4. Convenience functions
        touch(fname)
        write(fname, "Some Content")
        file_ent_with_name = create_file_entity(
            name = "TestFile2",
            local_path = upload_path,
            remote_path = string("/Inttests/", fname, "-2"),
        )
        results = insert_entity(file_ent_with_name)

        @test length(results) == 1
        @test has_errors(results[1]) == false
        @test get_id(results[1]) != ""

        file_ent_with_name = execute_query("FIND TestFile")
        results =
            retrieve_and_download_file_by_id(get_id(file_ent_with_name[1]), download_path2)
        execute(single_insert_transaction)
        results = get_results(single_insert_transaction)

        @test length(results) == 1
        @test has_errors(results[1]) == false
        @test get_id(results[1]) != ""
        @test isfile(download_path2) == true

        # 5. Cleanup
        ent_with_name = execute_query("FIND TestFile")
        single_insert_transaction = create_transaction()
        add_delete_by_id(single_insert_transaction, get_id(ent_with_name[1]))
        execute(single_insert_transaction)

        ent_with_name = execute_query("FIND TestFile2")
        single_insert_transaction = create_transaction()
        add_delete_by_id(single_insert_transaction, get_id(ent_with_name[1]))
        execute(single_insert_transaction)

        rm(upload_path)
        rm(download_path)
        rm(download_path2)

    end

end
